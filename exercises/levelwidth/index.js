// --- Directions
// Given the root node of a tree, return
// an array where each element is the width
// of the tree at each level.
// --- Example
// Given:
//     0
//   / |  \
// 1   2   3
// |       |
// 4       5
// Answer: [1, 3, 2]

function levelWidth(root) {
	var arr = []
	var q = []
	var temp = []
	q.push([root,0]);
	
	while(q.length > 0)
	{
		var top = q.shift()
		node = top[0]
		level = top[1]
		if(arr.length == level)
			arr.push(1);
		else
			arr[level]+=1;
		for(char of node.children)
			q.push([char,level+1])
	}
	return arr;
}

module.exports = levelWidth;
