// --- Directions
// Write a function that accepts a string.  The function should
// capitalize the first letter of each word in the string then
// return the capitalized string.
// --- Examples
//   capitalize('a short sentence') --> 'A Short Sentence'
//   capitalize('a lazy fox') --> 'A Lazy Fox'
//   capitalize('look, it is working!') --> 'Look, It Is Working!'

function capitalize(str) {
	var flag = 0;
	var newString = "";
	for(idx in str)
	{
		if(flag == 0)
		{
			newString+= str.charAt(idx).toUpperCase();
			flag = 1;
		}
		else newString+=str[idx];
		if(str.charAt(idx)==' ')
			flag = 0;
	}
	return newString;
}
console.log(capitalize("hello world"));

module.exports = capitalize;
