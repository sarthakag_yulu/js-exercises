// --- Directions
// Given an integer, return an integer that is the reverse
// ordering of numbers.
// --- Examples
//   reverseInt(15) === 51
//   reverseInt(981) === 189
//   reverseInt(500) === 5
//   reverseInt(-15) === -51
//   reverseInt(-90) === -9

function reverseInt(n) {
	var ans = 0;
	var neg = (n<0)?-1:1;
	if(n<0)n*=-1;
	while(n>0)
	{
		ans = ans*10 + n%10;
		n = (n/10) >> 0;
	}
	return neg*ans;
}

module.exports = reverseInt;
