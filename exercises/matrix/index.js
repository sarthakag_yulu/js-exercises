// --- Directions
// Write a function that accepts an integer N
// and returns a NxN spiral matrix.
// --- Examples
//   matrix(2)
//     [[1, 2],
//     [4, 3]]
//   matrix(3)
//     [[1, 2, 3],
//     [8, 9, 4],
//     [7, 6, 5]]
//  matrix(4)
//     [[1,   2,  3, 4],
//     [12, 13, 14, 5],
//     [11, 16, 15, 6],
//     [10,  9,  8, 7]]

function matrix(n) {
	var spiral = new Array(n);
	for(var i = 0;i<n;i++)
		spiral[i] = new Array(n);
	var r = 0, c = 0,rn = n, cn = n, cnt = 1;
	while(r<n && c<n)
	{
		for(var i = c;i<cn;i++)
			spiral[r][i] = cnt++;
		r++;
		for(var i = r;i<rn;i++)
			spiral[i][cn-1] = cnt++;
		cn--;
		for(var i = cn-1;i>=c;i--)
			spiral[rn-1][i] = cnt++;
		rn--;
		for(var i = rn-1;i>=r;i--)
			spiral[i][c] = cnt++;
		c++;
	}
	return spiral;
}

module.exports = matrix;
