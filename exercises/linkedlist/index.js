// --- Directions
// Implement classes Node and Linked Lists
// See 'directions' document

class Node {
	constructor(data, node = null)
	{
		this.data = data;
		this.next = node;
	}
}

class LinkedList {
	constructor()
	{
		this.head = null;
	}
	clear()
	{
		this.head = null;
	}
	size()
	{
		var cnt = 0;
		var node = this.head;
		while(node) {
			node = node.next;	
			cnt+=1;	
		}
		return cnt;
	}
	getAt(index) {
		var cnt = 0;
		var node = this.head;
		while(node) {
			if(cnt == index) 
				return node;
			node = node.next;	
			cnt+=1;	
		}
		return null;
	}

	getFirst() {
		return this.getAt(0);
	}

	getLast() {
		return this.getAt(this.size() - 1);
	}
	insertAt(data,pos)
	{
		if(!this.head)
		{
			this.head = new Node(data);
			return;
		}
		if(pos == 0)
		{
			this.head = new Node(data,this.head)
		}
		var prev;
		if(this.size()<pos-1)
			prev = getLast
		else prev = this.getAt(pos-1);
		var newNode = new Node(data,prev.next);
		prev.next = newNode;
	}
	insertFirst(data) {
		return this.insertAt(data, 0);
	}
	insertLast(data) {
		return this.insertAt(data, this.size());
	}
	removeAt(index) {
		if (!this.head) return;
		if (index === 0) {
			this.head = this.head.next;
			return;
		}
		const prev = this.getAt(index - 1);
		if (!prev || !prev.next) return;
		prev.next = prev.next.next;
	}

	removeFirst() {
		return this.removeAt(0);
	}

	removeLast() {
		return this.removeAt(this.size() - 1);
	}

}

module.exports = { Node, LinkedList };
