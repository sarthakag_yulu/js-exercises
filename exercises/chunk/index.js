// --- Directions
// Given an array and chunk size, divide the array into many subarrays
// where each subarray is of length size
// --- Examples
// chunk([1, 2, 3, 4], 2) --> [[ 1, 2], [3, 4]]
// chunk([1, 2, 3, 4, 5], 2) --> [[ 1, 2], [3, 4], [5]]
// chunk([1, 2, 3, 4, 5, 6, 7, 8], 3) --> [[ 1, 2, 3], [4, 5, 6], [7, 8]]
// chunk([1, 2, 3, 4, 5], 4) --> [[ 1, 2, 3, 4], [5]]
// chunk([1, 2, 3, 4, 5], 10) --> [[ 1, 2, 3, 4, 5]]

function chunk(array, size) {

	var c = 0;
	var A = []
	var Ans = []
	for(idx in array)
	{
		c+=1;
		A.push(array[idx]);
		if(c == size)
		{
			Ans.push(A);
			A = [];
			c = 0;
		}
	}
	if(A.length!=0)
		Ans.push(A);
	return Ans;
}

module.exports = chunk;
