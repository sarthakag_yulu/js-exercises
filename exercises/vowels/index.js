// --- Directions
// Write a function that returns the number of vowels
// used in a string.  Vowels are the characters 'a', 'e'
// 'i', 'o', and 'u'.
// --- Examples
//   vowels('Hi There!') --> 3
//   vowels('Why do you ask?') --> 4
//   vowels('Why?') --> 0

function vowels(str) {
	var cnt = 0;
	for(char of str)
	{
		if(char.toLowerCase() == 'a' || char.toLowerCase() == 'i'||char.toLowerCase() == 'o' || char.toLowerCase() == 'e'||char.toLowerCase() == 'u')
			cnt++;
	}
	return cnt;
}

module.exports = vowels;
