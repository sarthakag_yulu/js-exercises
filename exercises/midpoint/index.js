// --- Directions
// Return the 'middle' node of a linked list.
// If the list has an even number of elements, return
// the node at the end of the first half of the list.
// *Do not* use a counter variable, *do not* retrieve
// the size of the list, and only iterate
// through the list one time.
// --- Example
//   const l = new LinkedList();
//   l.insertLast('a')
//   l.insertLast('b')
//   l.insertLast('c')
//   midpoint(l); // returns { data: 'b' }

function midpoint(list) {
	var slow = list.head;
	var fast = list.head;
	var prev = list.head;
	var len = 0;
	while(fast)
	{
		len+=1;
		prev = slow;
		slow = slow.next;
		fast = fast.next;
		if(fast)
			fast = fast.next, len+=1;
	}

	return prev;
}

module.exports = midpoint;
