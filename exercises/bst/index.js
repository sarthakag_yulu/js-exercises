// --- Directions
// 1) Implement the Node class to create
// a binary search tree.  The constructor
// should initialize values 'data', 'left',
// and 'right'.
// 2) Implement the 'insert' method for the
// Node class.  Insert should accept an argument
// 'data', then create an insert a new node
// at the appropriate location in the tree.
// 3) Implement the 'contains' method for the Node
// class.  Contains should accept a 'data' argument
// and return the Node in the tree with the same value.

class Node {
	constructor(data)
	{
		this.left = null;
		this.right = null;
		this.data = data;
	}
	insert(data)
	{
		var newNode = new Node(data);
		this.insertNode(this, newNode);
	}
	insertNode(root, newNode)
	{
		if(newNode.data < root.data)
		{
			if(root.left == null)
				root.left = newNode;
			else 
				this.insertNode(root.left,newNode);
		}
		else
		{
			if(root.right == null)
				root.right = newNode;
			else 
				this.insertNode(root.right,newNode);
		}
	}
	contains(data)
	{
		var iterNode = this
		while(iterNode!=null)
		{
			if(iterNode.data > data )
				iterNode = iterNode.left;
			else if( iterNode.data < data)
				iterNode = iterNode.right;
			else return iterNode;
		}
		return null;

	}
}
module.exports = Node;
