// --- Directions
// Implement bubbleSort, selectionSort, and mergeSort

function bubbleSort(arr) {
	for(i = 0;i<arr.length;i++)
	{
		for( j = 0;j<arr.length-i-1;j++)
		{
			if(arr[j]>arr[j+1])
			{
				var temp = arr[j];
				arr[j] = arr[j+1]
				arr[j+1] = temp;
			}
		}
	}
	return arr;
}

function selectionSort(arr) {

	for(i = 0;i<arr.length;i++)
	{
		var mn = i;
		for(j = i+1;j<arr.length;j++)
		{
			if(arr[mn]>arr[j])
				mn = j;
		}
		var temp = arr[i];
		arr[i] = arr[mn]
		arr[mn] = temp;
	}
	return arr;

}

function mergeSort(arr) {

	if(arr.length<=1)return arr;
	var mid = (arr.length/2) >>0
	const left = arr.slice(0,mid)
	const right = arr.slice(mid)
	return merge(mergeSort(left),mergeSort(right));
}

function merge(left, right) {
	var i = 0, j = 0;
	var ans = [];
	while(i<left.length && j < right.length)
	{
		if(left[i]<right[j])ans.push(left[i++]);
		else ans.push(right[j++]);
	}
	while(i<left.length)
		ans.push(left[i++])
	while(j<right.length)
		ans.push(right[j++])
	return ans;

}

module.exports = { bubbleSort, selectionSort, mergeSort, merge };
