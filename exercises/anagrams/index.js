// --- Directions
// Check to see if two provided strings are anagrams of eachother.
// One string is an anagram of another if it uses the same characters
// in the same quantity. Only consider characters, not spaces
// or punctuation.  Consider capital letters to be the same as lower case
// --- Examples
//   anagrams('rail safety', 'fairy tales') --> True
//   anagrams('RAIL! SAFETY!', 'fairy tales') --> True
//   anagrams('Hi there', 'Bye there') --> False

function anagrams(stringA, stringB) {

	let freqA = {};
	let freqB = {};
	stringA = stringA.replace(/[^\w]/g, "").toLowerCase()
	stringB = stringB.replace(/[^\w]/g, "").toLowerCase()
	for( var char of stringA)
	{
		freqA[char] = freqA[char] + 1 || 1;
	}
	for( var char of stringB)
		freqB[char] = freqB[char] + 1 || 1;

	for(var char in freqB)
	{
		if(freqA[char]!= freqB[char])
			return false;
	}

	return true;
}

module.exports = anagrams;
