// --- Directions
// Implement a Queue datastructure using two stacks.
// *Do not* create an array inside of the 'Queue' class.
// Queue should implement the methods 'add', 'remove', and 'peek'.
// For a reminder on what each method does, look back
// at the Queue exercise.
// --- Examples
//     const q = new Queue();
//     q.add(1);
//     q.add(2);
//     q.peek();  // returns 1
//     q.remove(); // returns 1
//     q.remove(); // returns 2

const Stack = require('./stack');

class Queue {
	constructor()
	{
		this.one = new Stack();
		this.two = new Stack();
	}
	add(data)
	{
		this.one.push(data);
	}
	remove()
	{
		this.transfer12();
		var data = this.two.pop();
		this.transfer21();
		return data;
	}
	peek()
	{
		this.transfer12();
		var data = this.two.peek();
		this.transfer21();
		return data;
	}
	transfer12()
	{
		while(this.one.peek())this.two.push(this.one.pop());
	}
	transfer21()
	{
		while(this.two.peek())this.one.push(this.two.pop());
	}
}

module.exports = Queue;
