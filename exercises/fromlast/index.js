// --- Directions
// Given a linked list, return the element n spaces
// from the last node in the list.  Do not call the 'size'
// method of the linked list.  Assume that n will always
// be less than the length of the list.
// --- Examples
//    const list = new List();
//    list.insertLast('a');
//    list.insertLast('b');
//    list.insertLast('c');
//    list.insertLast('d');
//    fromLast(list, 2).data // 'b'

function fromLast(list, n) {
	var len = 0;
	var root = list.head;
	while(root!=null)
	{
		root = root.next
		len+=1;
	}
	var required = len-n-1;
	root = list.head
	for(var i = 0;i<required;i++)
		root = root.next
	return root
}

module.exports = fromLast;
