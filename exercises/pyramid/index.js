// --- Directions
// Write a function that accepts a positive number N.
// The function should console log a pyramid shape
// with N levels using the # character.  Make sure the
// pyramid has spaces on both the left *and* right hand sides
// --- Examples
//   pyramid(1)
//       '#'
//   pyramid(2)
//       ' # '
//       '###'
//   pyramid(3)
//       '  #  '
//       ' ### '
//       '#####'

function pyramid(n) {
	for(var i = 0;i<n;i++)
	{
		var str = ""
		for(var j = 0;j<n-i-1;j++)str+=' '
		for(var j = 0;j<i*2 + 1;j++)str+='#';
		for(var j = 0;j<n-i-1;j++)str+=' '

		console.log(str);
	}
}

module.exports = pyramid;
